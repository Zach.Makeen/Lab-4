package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(3, 7);
		shapes[1] = new Rectangle(6, 9);
		shapes[2] = new Circle(2);
		shapes[3] = new Circle(8);
		shapes[4] = new Square(5);
		for(int i = 0; i < shapes.length; i++) {
			System.out.println("The area is: " + shapes[i].getArea() + " and the perimeter is: " + shapes[i].getPerimeter());
			
		}
		
	}
	
}
