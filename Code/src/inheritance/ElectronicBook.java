package inheritance;

public class ElectronicBook extends Book {
	public int numberBytes;
	public ElectronicBook(String author, String title, int numberBytes) {
		super(author, title);
		this.numberBytes = numberBytes;
		
	}
	public String toString() {
		return (super.toString() + " which contains " + numberBytes + " bytes.");
		
	}
}