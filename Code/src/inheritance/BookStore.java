package inheritance;

public class BookStore {
	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0] = new ElectronicBook("Joe Mama", "Cooking with Joe", 10);
		books[1] = new Book("Zach", "How to Code Java");
		books[2] = new ElectronicBook("Rodrigo", "How to be a Dad", 24);
		books[3] = new Book("Angela", "How to be a 90s Kid");
		books[4] = new Book("Julian", "How to Get Covid");
		for(int i = 0; i < books.length; i++) {
			System.out.println(books[i]);
		}
		
	}
	
}