package inheritance;

public class Book {
	protected String title;
	private String author;
	public Book(String author, String title) {
		this.author = author;
		this.title = title;
		
	}
	public String getTitle() {
		return this.title;
		
	}
	public String getAuthor() {
		return this.author;
		
	}
	public String toString() {
		return (title + " is written by " + author);
		
	}
}